
const Hapi = require('hapi');
const _ = require('lodash');
const MongoClient = require('mongodb').MongoClient;

// process args
options = {
    port: process.argv[2] || 18080,
    host: process.argv[3]
};

// connect to db
MongoClient.connect('mongodb://localhost:27017/chrinorChipRegistry', function(err, db) {
    if(err) {
        return console.dir(err);
    }

    db.createCollection('registry', function(err, collection) {
        if (err) {
            throw err;
        }
    });
    const registry = db.collection('registry');


    const server = new Hapi.Server();
    server.connection(_.extend({}, options, {}));

    server.route({
        method: 'GET',
        path: '/hello',
        handler: function (request, reply) {
            reply({'status': 'ok'});
        }
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: function (request, reply) {
            registry.find({}).toArray().then((results) => {
                var body = '<html>';
                body += '<head>';
                body += '<meta http-equiv="refresh" content="10"><title>Known C.H.I.P. IPs</title>';
                body += '<style>table { border: 1px solid black; } td { border: 1px solid black; padding: 0.25em; }</style>';
                body += '</head>';
                body += '<body><table>';
                body += '<tr><th>Address</th><th>Last Seen</th></tr>';
                _.each(results, (result) => {
                    body += `<tr><td>${result.remoteAddress}</td><td>${result.created}</td></tr>`;
                });
                body += '</table></body>';
                body += '<html>';

                return body;
            }).then(reply);
        }
    });

    server.route({
        method: 'GET',
        path: '/checkin',
        handler: function (request, reply) {

            // submit to mongo db
            registry.updateOne({
                remoteAddress: request.info.remoteAddress
            }, _.extend({}, request.info, {
                created: new Date()
            }), {
                upsert: true,
                w: 1
            }, function(err, result) {
                if (result.result.nModified) {
                    console.log(`Checkin from ${request.info.remoteAddress}`);
                } else {
                    console.log(`Registered ${request.info.remoteAddress}`);
                }
                reply({status: 'ok'});
            });
        }
    });

    server.start((err) => {
        if (err) {
            throw err;
        }

        console.log(`Server running at: ${server.info.uri}`);

        function checkDead() {
            // query DB for last time
            registry.find({}).toArray().then((result) => {
                _.each(result, (result) => {
                    var now = new Date() - (60000 * 2);
                    if (result.created < now) {
                        console.log(`Unregistering ${result.remoteAddress}`);
                        registry.remove(result);
                    }
                });
            }).then(() => {
                setTimeout(checkDead, 5000);
            });

        }

        checkDead();
    });

});
